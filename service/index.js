const BASE_URL = "http://123.207.32.32:9001"
const LOGIN_BASE_URL ="http://123.207.32.32:3000"
import{token_key} from '../const/token'
const token = wx.getStorageSync(token_key)
class JJrequest{
    constructor(baseurl,tokenHeader={}){
        this.baseurl=baseurl
        this.tokenHeader=tokenHeader
    }
    request(url,method,params,isAuth=false,header = {}){
  const finalHeader = isAuth ? {...this.tokenHeader,...header} : header
        return new Promise((resolve,reject)=>{
            wx.request({
                url: this.baseurl +url,
                method:method,
                data:params,
                header:finalHeader,
                success:(res)=>{
                resolve(res.data)
                },
                fail:(err)=>{
                    reject(err)
                }
              })
        }) 
        
    }
     get(url,params,isAuth=false,header){
       return this.request(url,"GET",params,isAuth,header)
    }
    post(url,data,isAuth=false,header){
        return this.request(url,"POST",data,isAuth,header)
    }

    
}
const jjrequest=new JJrequest(BASE_URL)
const jjLoginrequest=new JJrequest(LOGIN_BASE_URL,{token})
export default jjrequest
export{  
    jjLoginrequest
}
