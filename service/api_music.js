import jjrequest from "./index"
export function getBanner(type) {
    return jjrequest.get("/banner",{type})
}
export  function getPlaylistDetail(type="0") {
 return jjrequest.get("/top/song",{type})
}
export function getRings(){
    return jjrequest.get("/toplist/detail")
}
export function getTopPlaylist(cat="全部",limit=6,offset=10){
  return jjrequest.get("/top/playlist",{
      limit,
      offset,
      cat
  })
}
export function getplaylist(){
    return jjrequest.get("/toplist")
}
export function getSongDetail(id) {
return jjrequest.get("/playlist/detail/dynamic",{id})
}
export function getSearchHot() {
    return jjrequest.get("/search/hot")
}
export function getSearchSuggest(keywords) {
    return jjrequest.get("/search/suggest", {
      keywords,
      type: "mobile"
    })
}
export function searchKeywords(keywords) {
    return jjrequest.get("/search",{
        keywords
    })
}