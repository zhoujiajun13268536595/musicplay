import jjrequest from './index'
export function getTopMv(offset,limit=10){
return jjrequest.get("/top/mv",{offset,limit})
}
export function getMVURL(id) {
    return jjrequest.get("/mv/url", {
      id
    })
  }
  export function getMVDetail(mvid) {
    return jjrequest.get("/mv/detail", {
      mvid
    })
  }
  export function getRelatedVideo(id) {
    return jjrequest.get("/related/allvideo", {
      id
    })
  }
  