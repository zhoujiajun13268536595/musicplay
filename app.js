// app.js
import { getLogincode ,sendCodeToserver,checkToken,checkSession} from "./service/api_login"
import {token_key} from './const/token'
App({
    globalData: {
        screenWidth: 0,
        screenHeight: 0,
        statusBarHeight: 0,
        navBarHeight: 44
      },
async onLaunch(){
    const info = wx.getSystemInfoSync({
        success (res) {
        }
    })
    this.globalData.statusBarHeight = info.statusBarHeight
    this.globalData.screenWidth = info.screenWidth
    this.globalData.screenHeight = info.screenHeight
    const token = wx.getStorageSync(token_key)
    const check= await checkToken(token)
    const isSessionExpire = await checkSession()
    if(!token || check.errorCode|| !isSessionExpire){
      this.loginAction()  
    }
},

 loginAction : async function(){
     const code = await getLogincode()
     const result=await sendCodeToserver(code)
      const token= result.token
    wx.setStorageSync('token_key', token)
 }
})
