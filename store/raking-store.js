import { HYEventStore } from 'hy-event-store'
import { getPlaylistDetail,getRings} from '../service/api_music'

const rankingStore = new HYEventStore({
  state: {
    hotRanking: [],
    hotKing:[],
    playListSongs :[],
    playListIndex : 0,
    playingname :"pause",
    isplaying: true
  },
  actions: {
    getRankingDataAction(ctx) {
        getPlaylistDetail().then(res =>{
            ctx.hotRanking = res.data
        })
    },
    getKingRing(ctx) {
        getRings().then(res =>{
           ctx.hotKing=res.list
        })
    }
  }
})

export {
  rankingStore
}