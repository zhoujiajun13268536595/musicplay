// pages/home-vedio/index.js
import { getTopMv } from "../../service/api_vedio"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        topMVs: [],
        hasMore: true
    },

    /**
     * 生命周期函数--监听页面加载
     */ 
    onLoad:async function (options) {
    const res=await getTopMv(0)
    this.setData({topMVs : res.data})
},
  getTopMvData :async function(offset){
     if(!this.data.hasMore) return
     //展示加载动画
     wx.showNavigationBarLoading()
    const res=await getTopMv(offset)
    let newData = this.data.topMVs
    if(offset == 0 ){
        newData= this.data.topMVs
    }else {
        newData= newData.concat(res.data)
    }
     this.setData({topMVs:newData})
     this.setData({hasMore:res.hasMore})
     wx.hideNavigationBarLoading()
    if (offset === 0) {
      wx.stopPullDownRefresh()
    }
 },
 // 封装事件处理的方法
 handleVideoItemClick: function(event) {
    // 获取id
    const id = event.currentTarget.dataset.item.id
    // 页面跳转
    wx.navigateTo({
      url: `/pages/detail-video/index?id=${id}`,
    })
  },
 // 其他的生命周期回调函数
 onPullDownRefresh: async function() {
    this.getTopMvData(0)
  },
   onReachBottom: async function() {
    this.getTopMvData(this.data.topMVs.length)
  }
})