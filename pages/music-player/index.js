// pages/music-player/index.js
import {getLyric}from "../../service/api-play"
import {audioContext} from '../../utils/audio-play'
import{parselyric} from "../../utils/getlyric"
import{rankingStore}from "../../store/raking-store"
const playModeNames=["order","repeat","random"]


Page({
    data: {
     isstoping:false,
     isplay:true,
      id: 0,
      durationstime:"",
      value : 0,
      lyric:[],
      currentSong: {},
      durationstime:0,
      name:"",
      currentPage: 0,
      contentHeight: 0,
      currentTime:0,
      lyrictext :"",
      currentLyricIndex:0,
      currentLyricIndexTop:0,
      playModeIndex:0,
      playModeNames:"order",
      playingname: "pause",
      isplaying :true,
      playListSongs:[],
      playListIndex:0
    },
    onLoad: function (options) {
        rankingStore.setState('isplaying',this.data.isplaying)
        const playListIndex = rankingStore.state.playListIndex
        const playListSongs = rankingStore.state.playListSongs
        this.setData({playListIndex})
        this.setData({playListSongs})
         this.setData({name :playListSongs[playListIndex].name})
        audioContext.title =this.data.name
        const id = options.id
        this.setData({id})
      const globalData = getApp().globalData
      const screenHeight = globalData.screenHeight
      const statusBarHeight = globalData.statusBarHeight
      const navBarHeight = globalData.navBarHeight
      const contentHeight = screenHeight - statusBarHeight - navBarHeight
      this.setData({ contentHeight })
      if(this.data.id ==options.id)
      //发送网络请求
     this.handelegetLyric(id)
    //   创建播放
    if(this.data.isplaying && this.data.isstoping){
        audioContext.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3`  
        audioContext.title =this.data.name
        this.setData({isplaying : false})
    }
    audioContext.play()
     audioContext.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3`   
    audioContext.autoplay=true
      if(this.data.isplay){
        audioContext.onCanplay(()=>{
            audioContext.play()
       })
       audioContext.onTimeUpdate(()=>{   
            const durationstime =audioContext.duration *1000
            const currentTime =audioContext.currentTime *1000
            this.setData({currentTime})
            this.setData({durationstime})
            const vaule =currentTime/durationstime *100
            this.setData({value:vaule})
            for (let i=0;i<this.data.lyric.length;i++){
                const lyricInfos= this.data.lyric[i]
               if (currentTime < lyricInfos.millsecond) {
                   const currentIndex = i-1
                  if (this.data.currentLyricIndex !==currentIndex){
                    const currenlyric = this.data.lyric[currentIndex]
                    const  currentLyricIndexTop =this.data.currentLyricIndex *35
                      this.setData({lyrictext :currenlyric.text ,currentLyricIndex :currentIndex,currentLyricIndexTop})
                  }
                     break
               }
            }
        })
        audioContext.onEnded(()=>{
        this.handleNextBtnclick()
        })
        audioContext.onPlay(()=>{
                this.setData({isplaying : true})
        }),
        audioContext.onPause(()=>{
         if(this.data.playingname ==="resume"){
        this.setData({isplaying : false})
        audioContext.pause()
    }
        })
        audioContext.onStop(()=>{
          this.setData({isplaying : false})
          this.setData({isstoping:true})
        })
        this.setData({isplay : !this.data.isplay})

      }
     
  },
    // 发送网络请求
    handelegetLyric:function(id) {
      return getLyric(id).then(res=>{
        const lyricString =res.lrc.lyric
       const lyric=parselyric(lyricString)
       this.setData({lyric})
   })
    },
    // 事件处理
    handleSwiperChange: function(event) {
      const current = event.detail.current
      this.setData({ currentPage: current })
    },
    handleSliderChange(event){
        const value=event.detail.value
        const currentTime =  this.data.durationstime * value /100
        audioContext.pause()
        audioContext.seek(currentTime /1000)
        this.setData({value})
    },
    handnavback(event){
        // audioContext.pause()
         wx.navigateBack({
         delta: 1,
        })
        
 },
handleMOdeBtnclick(event){
        let playModeIndex=this.data.playModeIndex+1
        if(playModeIndex === 3) playModeIndex = 0
        this.setData({playModeIndex,playModeNames:playModeNames[playModeIndex]})
      },
 handleplaynameBtnclick() {
  const isplaying =this.data.isplaying
  const playingname =  isplaying ? "pause":"resume"
  this.setData({isplaying:!isplaying, playingname})
  rankingStore.setState("playingname", playingname)
  rankingStore.setState("isplaying", isplaying)
  if(isplaying === false){
    audioContext.pause()
 }else {
    audioContext.play()
 }
},
//上一首歌曲
handlePreviousBtnClick (event){
    let index =rankingStore.state.playListIndex
    const playListSongs = this.data.playListSongs
    switch(this.data.playModeIndex){
         case 0:
             index = index - 1
             if(index === -1) 
             index=playListSongs.length - 1
             break
             case 1 : 
             index =index
             break
             case 2 :
            index = Math.floor(Math.random()*playListSongs.length)
            break
    }
    rankingStore.setState("playListIndex",index)
    this.setData({playListIndex:index})  
    let id = playListSongs[index].id
    let name =playListSongs[index].name
     this.setData({id})
     this.setData({name})
    this.handelegetLyric(id)
     audioContext.title =this.data.name
    audioContext.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3`       
},
//下一首歌曲
handleNextBtnclick(event){
let index =rankingStore.state.playListIndex
const playListSongs = this.data.playListSongs
switch(this.data.playModeIndex){
     case 0:
         index = index + 1
         if(index === playListSongs.length) 
         index = 0
         break
         case 1 : 
         index =index
         break
         case 2 :
            index = Math.floor(Math.random()*playListSongs.length)
         break
 }

   rankingStore.setState("playListIndex",index)
   this.setData({playListIndex:index})  
   let id = playListSongs[index].id
   let name =playListSongs[index].name
    this.setData({id})
    this.setData({name})
  this.handelegetLyric(id)
    audioContext.title =this.data.name
    audioContext.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3` 
},
onUnload: function () {
}
  })
 

