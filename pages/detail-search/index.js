// pages/detail-search/index.js
import{getSearchHot,getSearchSuggest,searchKeywords} from "../../service/api_music"
import{rankingStore}from "../../store/raking-store"
import debounce from "../../utils/debounce"
const  debounceGetSearchSuggest = debounce(getSearchSuggest,300)
Page({

    /**
     * 页面的初始数据
     */
    data: {
        hotKeywords:{},
        suggestSongs: [],
         searchValue: "",
         resultSongs:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
      // 1.获取页面的数据
      this.getPageData()
    },
    getPageData: function() {
        getSearchHot().then(res => {
          this.setData({ hotKeywords: res.result.hots })
        })
      },
      handleSearchChange: function(event) {
        const searchValue = event.detail
        this.setData({ searchValue })
        if (!searchValue.length) {
          this.setData({ suggestSongs: [] })
          this.setData({resultSongs:[]})
          debounceGetSearchSuggest.cancel()
          return
        }
        debounceGetSearchSuggest(this.data.searchValue).then(res => {
          this.setData({ suggestSongs: res.result.allMatch })
        })
      },
      handleSearch(){
          const searchValue = this.data.searchValue
          searchKeywords(searchValue).then(res =>{
            this.setData({resultSongs : res.result.songs})
          }
        )
      },
      handsuggestSongsClick(event){
         const item = event.currentTarget.dataset.item.keyword
         this.setData({searchValue : item})
         searchKeywords(item).then(res =>{
            this.setData({resultSongs : res.result.songs})
          }
        )
          },
          handHotClick(event){
             const searchValue=event.currentTarget.dataset.first
             this.setData({searchValue})
             searchKeywords(searchValue).then(res =>{
                this.setData({resultSongs : res.result.songs})
              }
            )
             
          },
          handSongClick(event){
            const index= event.detail
            rankingStore.setState("playListSongs", this.data.resultSongs)
            rankingStore.setState("playListIndex",index)
          }
        }
        )
 