// pages/detail-songs/index.js
import {rankingStore} from "../../store/raking-store"
import{getSongDetail}from "../../service/api_music"
Page({
    data: {
        isfor:"",
       ranking:{},
       recommendMusic:[],
       menu:{}
    },
    onLoad(options) {   
         const type = options.type
         this.setData({isfor:type})
            if(type === "rank"){
             const id = options.ranking
            rankingStore.onState("hotKing",(res)=>{
                this.setData({ranking:res[id]})
            }) 
            this.data.isfor =true
            }else if (type==="more") {
               rankingStore.onState("hotRanking",(res)=>{
                    this.setData({recommendMusic:res})
                   
            })}else if(type==="menu"){
                const id = options.id
                getSongDetail(id).then(res=>{
                    this.setData({menu:res.playlist})
                })
            }         
},
handleSongItemClick1(event){
    const index =event.detail
    rankingStore.setState("playListSongs", this.data.menu.tracks)
    rankingStore.setState("playListIndex",index)
},
handleSongItemClick2(event){
    const index =event.detail
    rankingStore.setState("playListSongs", this.data.recommendMusic)
    rankingStore.setState("playListIndex",index)
}
})