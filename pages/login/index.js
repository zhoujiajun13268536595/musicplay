// pages/login/index.js
import {getlogin} from "../../service/api_login"

Page({
    data: {
      info :{},
      isshow:true
    },
    onLoad(options) {
    
    },
   getUserinfo(event){      
       const isshow = !this.data.isshow
       this.setData({isshow})
     getlogin().then(res=>{
        this.setData({info : res.userInfo})
     })
  }
})
    