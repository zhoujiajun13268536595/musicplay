// pages/home-music/index.js
import {getBanner,getTopPlaylist,getplaylist} from "../../service/api_music"
import{querySelect}from "../../utils/query-select"
import {throttle} from "../../utils/throttle"
import {rankingStore}from "../../store/raking-store"
import {audioContext} from '../../utils/audio-play'
const throttleQuery = throttle(querySelect,1000,{trailing : true})
Page({  
    data: {
    swiperHeight:0,
    banners: [],
    hotMusic:"",
    recommendSong:[],
    gethostlist:[],
    getRecommendList:[],
    hotid:[],
    playListSongs:[],
    playListIndex:0,
    isplaying:false,
    playingname: "pause",
    playAnimState:"running",
    },
    onLoad(options) {
    rankingStore.onState('isplaying', res=>{
            this.setData({isplaying :res})
         })
    getBanner(2).then(res=>{
      this.setData({banners: res.banners})
    }),
    getTopPlaylist().then(res=>{
        this.setData({gethostlist:res.playlists})
    }), 
    getTopPlaylist("流行").then(res=>{
        this.setData({getRecommendList:res.playlists})
    })
    
  rankingStore.dispatch("getRankingDataAction")
  rankingStore.onState("hotRanking",(res)=>{
      const recommendSong = res.slice(0,6)
      this.setData({recommendSong:recommendSong})
  })
  rankingStore.dispatch("getKingRing")
  rankingStore.onState("hotKing",(res)=>{
      const hotMusic = res.slice(0,3)
      this.setData({hotMusic:hotMusic})
  })
  rankingStore.onState("playListSongs",(res)=>{
      this.setData({playListSongs : res})
  })
  rankingStore.onState("playListIndex",(res)=>{
    this.setData({playListIndex : res})
})
},
 handleSearchClick: function() {
        wx.navigateTo({
          url: '/pages/detail-search/index?id=',
        })
      },
      handleSwiperImageLoaded(){
        throttleQuery('.swiper-image').then(res=>{
            const rec = res[0]
        this.setData({swiperHeight:rec.height})
     })
      },
      handMoreClick(){
        wx.navigateTo({
            url: `/pages/detail-songs/index?ranking=more&type=more`
          })
      },
      handleRankingClick(event){
          const id = event.currentTarget.dataset.id
         this.navigateToDetailSongsPage(id)
      },
      navigateToDetailSongsPage: function(id){
        wx.navigateTo({
            url: `/pages/detail-songs/index?ranking=${id}&type=rank`
          })
      },
      handleSongItemClick(event){
          const index= event.currentTarget.dataset.index
          rankingStore.setState("playListSongs", this.data.recommendSong)
          rankingStore.setState("playListIndex",index)
     },
     handlePlayBtnClick: function(event){
    rankingStore.setState('isplaying' , !this.data.isplaying)
       if(this.data.isplaying === true){
           audioContext.play()
       }else{
           audioContext.pause()
       }
        
},
handleplaybar(){
    rankingStore.onState('playListSongs',(res=>{
        this.setData({playListSongs:res})
    }))
    rankingStore.onState('playListIndex',res=>{
      this.setData({playListIndex:res})
    })
    let id = this.data.playListSongs[this.data.playListIndex].id 
     wx.navigateTo({
      url: '/pages/music-player/index?id='+id,
    })
   
}
})