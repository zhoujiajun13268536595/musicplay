const timeReg = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/
export function parselyric(lyricString){
    const lyric= lyricString.split("\n")
    const lyricInfos =[]
    for (const lineString of lyric){
         const time = timeReg.exec(lineString)
         if (!time) continue
        const minute = time[1] *60 *1000
        const second = time[2]*1000
        const millsecondTime= time[3].length==2 ? time[3]*10 :time[3]*1
        const millsecond =minute+second+millsecondTime
        const text = lineString.replace(timeReg,"")
        lyricInfos.push({millsecond,text})
    }
    return lyricInfos
}