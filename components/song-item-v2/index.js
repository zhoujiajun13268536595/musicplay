// components/song-item-v2/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        ranking :{
          type:Object,
          value:{}
      }
    },
    data: {

    },
    methods: {
        handlesongItemClick(){
            const id = this.properties.item.id
            wx.navigateTo({
              url: `/pages/music-player/index?id=${id}&type=recommend`,
            })
           }
    }
})
