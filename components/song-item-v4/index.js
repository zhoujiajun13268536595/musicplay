// components/song-item-v4/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        menu :{
            type:Object,
            value:{}
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        handlesongItemClick(event){ 
            const index =event.currentTarget.dataset.index
            this.triggerEvent("click",index)
            const id =event.currentTarget.dataset.id
            wx.navigateTo({
             url: `/pages/music-player/index?id=${id}`,
           })
         }
        

    }
})
